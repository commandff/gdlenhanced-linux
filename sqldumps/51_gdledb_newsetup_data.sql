-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `gdle`;

-- Dumping data for table gdle.config: ~42 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`setting`, `runtime`, `base`, `gameuse`) VALUES
	('allow_general_chat', NULL, '1', 'Allow players to use General chat channel'),
	('kill_xp_multiplier', '1.000000', '1.0', 'Mutliplier for mob kill xp'),
	('reward_xp_multiplier', NULL, '1.0', 'Mutliplier for quest reward xp hand in'),
	('lum_xp_multipler', NULL, '1.0', 'Multiplier for luminance earnings'),
	('drop_rate_multiplier', NULL, '1.0', 'Mutliplier for mob trophy drops'),
	('respawn_time_multiplier', NULL, '1.0', 'Alters the speed of generator respawns.  Less than 1.0 faster as percentage'),
	('speed_hack_kick_threshold', NULL, '1.2', 'If speed kick is enabled, the frequency of messages to try to detect GEAR users'),
	('void_damage_reduction', NULL, '1.0', 'Alters the damage percentage of void in PvP.  E.g. 0.50 = 50% damage'),
	('rare_drop_multiplier', NULL, '0.0', 'Rate at which to drop rares.  Rate to be "close" to wiki is 0.15'),
	('missile_attr_adj', NULL, '0.0', 'Adjust Missile damage default. Negative for less damage, Positive for more'),
	('blue_sigil_rate', NULL, '0.005', 'Proc chance for Blue sigil effect per level of aetheria'),
	('yellow_sigil_rate', NULL, '0.0075', 'Proc chance for Yellow sigil effect per level of aetheria'),
	('red_sigil_rate', NULL, '0.01', 'Proc chance for Red sigil effect per level of aetheria'),
	('pk_cs_melee_base_chance', NULL, '0.1', 'PK base chance to proc Critical Strike imbue for melee'),
	('pk_cs_melee_min_skill', NULL, '150', 'PK Min skill needed to proc Critical Strike imbue for melee'),
	('pk_cs_melee_max_skill', NULL, '400', 'PK Max skill needed for max Critical Strike chance for melee'),
	('pk_cs_melee_max_chance', NULL, '0.5', 'PK max chance to proc Critical Strike imbue for melee'),
	('pk_cb_melee_base_mult', NULL, '1.0', 'PK base multiplier for Crippling Blow imbue for melee'),
	('pk_cb_melee_min_skill', NULL, '150', 'PK Min skill needed to proc Crippling Blow imbue for melee'),
	('pk_cb_melee_max_skill', NULL, '400', 'PK Max skill needed for max Crippling Blow chance for melee'),
	('pk_cb_melee_max_mult', NULL, '7.0', 'PK max multiplier for Crippling Blow imbue for melee'),
	('pk_cs_missile_base_chance', NULL, '0.1', 'PK base chance to proc Critical Strike imbue for missile'),
	('pk_cs_missile_min_skill', NULL, '125', 'PK Min skill needed to proc Critical Strike imbue for missile'),
	('pk_cs_missile_max_skill', NULL, '360', 'PK Max skill needed for max Critical Strike chance for missile'),
	('pk_cs_missile_max_chance', NULL, '0.5', 'PK max chance to proc Critical Strike imbue for missile'),
	('pk_cb_missile_base_mult', NULL, '1.0', 'PK base multiplier for Crippling Blow imbue for missile'),
	('pk_cb_missile_min_skill', NULL, '125', 'PK Min skill needed to proc Crippling Blow imbue for missile'),
	('pk_cb_missile_max_skill', NULL, '360', 'PK Max skill needed for max Crippling Blow chance for missile'),
	('pk_cb_missile_max_mult', NULL, '7.0', 'PK max multiplier for Crippling Blow imbue for missile'),
	('pk_cs_magic_base_chance', NULL, '0.05', 'PK base chance to proc Critical Strike imbue for magic'),
	('pk_cs_magic_min_skill', NULL, '125', 'PK Min skill needed to proc Critical Strike imbue for magic'),
	('pk_cs_magic_max_skill', NULL, '360', 'PK Max skill needed for max Critical Strike chance for magic'),
	('pk_cs_magic_max_chance', NULL, '0.25', 'PK max chance to proc Critical Strike imbue for magic'),
	('pk_cb_magic_base_mult', NULL, '0.5', 'PK base multiplier for Crippling Blow imbue for magic'),
	('pk_cb_magic_min_skill', NULL, '125', 'PK Min skill needed to proc Crippling Blow imbue for magic'),
	('pk_cb_magic_max_skill', NULL, '360', 'PK Max skill needed for max Crippling Blow chance for magic'),
	('pk_cb_magic_max_mult', NULL, '5', 'PK max multiplier for Crippling Blow imbue for magic'),
	('cloak_base_proc_rate', NULL, '0.05', 'Base proc chance for cloak special ability'),
	('cloak_half_hlth_proc_bonus', NULL, '0.075', 'Bonus proc chance if hit does greater than half players max health'),
	('cloak_qtr_hlth_proc_bonus', NULL, '0.05', 'Bonus proc chance if hit does greater than a quarter of players max health'),
	('cloak_tnth_hlth_proc_bonus', NULL, '0.025', 'Bonus proc chance if hit does greater than a tenth of players max health'),
	('cloak_per_level_bonus', NULL, '0.01', 'Bonus proc rate chance per level of cloak as it levels');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping data for table gdle.globals: ~5,784 rows (approximately)
/*!40000 ALTER TABLE `globals` DISABLE KEYS */;
INSERT INTO `globals` (`id`, `data`) VALUES
	(1, _binary 0x00000000),
	(2, _binary 0x00000000),
	(3, _binary 0x00000000);
/*!40000 ALTER TABLE `globals` ENABLE KEYS */;

-- Dumping data for table gdle.teletowns: ~59 rows (approximately)
/*!40000 ALTER TABLE `teletowns` DISABLE KEYS */;
INSERT INTO `teletowns` (`ID`, `Description`, `Command`, `Landblock`, `Position_X`, `Position_Y`, `Position_Z`, `Orientation_W`, `Orientation_X`, `Orientation_Y`, `Orientation_Z`) VALUES
	(1, 'Teletown - Aerlinthe', 'Aerlinthe Island', 'BAE8001D', '42A80000', '42D20000', '41D00A3D', '00000000', '00000000', '00000000', 'BF800000'),
	(2, 'Teletown - Ahurenga', 'Ahurenga', '0FB90009', '422C0000', '4109999A', '3BA3D70A', 'BF7AE7B4', '00000000', '00000000', 'BE4B46FE'),
	(3, 'Teletown - Al-Arqas', 'Al-Arqas', '8F58003B', '4337D9DB', '4270BB64', '411536F4', '3F3504F7', '00000000', '00000000', 'BF3504F7'),
	(4, 'Teletown - Al-Jalima', 'Al-Jalima', '8588002C', '42F0B7CF', '42BEF0A4', '42B4192C', '3F800000', '00000000', '00000000', '00000000'),
	(5, 'Teletown - Arwic', 'Arwic', 'C6A90009', '423B3852', '4087020C', '4228051F', '3F800000', '00000000', '00000000', '00000000'),
	(6, 'Teletown - Ayan Baqur', 'Ayan Baqur', '11340025', '42C79EE7', '42D7D1C7', '4228051F', '3F3603A3', '00000000', '00000000', 'BF3404DA'),
	(7, 'Teletown - Baishi', 'Baishi', 'CE410007', '4149999A', '4318CCCD', '425C3852', 'BF0B6D76', '00000000', '00000000', 'BF56B325'),
	(8, 'Teletown - Bandit Castle', 'Bandit Castle', 'BDD00006', '41873333', '42F10000', '42E63333', '3F3504F7', '00000000', '00000000', 'BF3504F7'),
	(9, 'Teletown - Beach Fort', 'Beach Fort', '42DE000C', '41C80000', '42A90000', '3BA3D70A', 'BF2E976C', '00000000', '00000000', 'BF3B3A04'),
	(10, 'Teletown - Bluespire', 'Bluespire', '21B00017', '4240C28F', '4325E3D7', '3BA3D70A', 'BDAB3F64', '00000000', '00000000', 'BF7F1A7E'),
	(11, 'Teletown - Candeth Keep', 'Candeth Keep', '2B11003D', '433D2358', '42C59A27', '4240051F', 'BF6DE29B', '00000000', '00000000', 'BEBD2C38'),
	(12, 'Teletown - Cragstone', 'Cragstone', 'BB9F0040', '43295BA6', '43284042', '4258051F', '3F142492', '00000000', '00000000', 'BF50C804'),
	(13, 'Teletown - Crater Lake Village', 'Mt Esper-Crater Village', '90D00107', '42BF0AC1', '42A80000', '438A9A3D', 'BF3504F7', '00000000', '00000000', 'BF3504F7'),
	(14, 'Teletown - Danby\'s Outpost', 'Danby\'s Outpost', '5A9C0004', '41BC0000', '429A3333', '40C028F6', '00000000', '00000000', '00000000', 'BF800000'),
	(15, 'Teletown - Dryreach', 'Dryreach', 'DB75003B', '433A0000', '42820000', '42100000', 'BF400000', '00000000', '00000000', '3F266666'),
	(16, 'Teletown - Eastham', 'Eastham', 'CE940035', '43170D91', '42E13852', '418B5687', 'BF6FC383', '00000000', '00000000', 'BEB36FA0'),
	(17, 'Teletown - Fort Tethana', 'Fort Tethana', '2681001D', '429B6666', '42D83333', '43700147', 'BF05C26E', '00000000', '00000000', 'BF5A469D'),
	(18, 'Teletown - Glenden Wood', 'Glenden Wood', 'A0A40025', '42C09AA0', '42EFB1AA', '426FD194', '3F3504F7', '00000000', '00000000', 'BF3504F7'),
	(19, 'Teletown - Greenspire', 'Greenspire', '2BB5003C', '4332F53F', '42AD23D7', '3BA3D70A', '3EB466F5', '00000000', '00000000', 'BF6F951C'),
	(20, 'Teletown - Hebian-to', 'Hebian-to', 'E64E002F', '430A4DD3', '4321E7AE', '41A05194', '3F6C8366', '00000000', '00000000', 'BEC3EF07'),
	(21, 'Teletown - Holtburg', 'Holtburg', 'A9B40019', '42A80000', '40E33333', '42BC0290', '3F7F35F4', '00000000', '00000000', 'BDA0AF1D'),
	(22, 'Teletown - Kara', 'Kara', 'BA170039', '43353333', '404CCCCD', '43279AE1', 'BF5919AC', '00000000', '00000000', 'BF07A8C6'),
	(23, 'Teletown - Khayyaban', 'Khayyaban', '9F44001A', '42B40000', '41C46C8B', '4212344F', 'BF4858FF', '00000000', '00000000', 'BF1F5D25'),
	(24, 'Teletown - Kryst', 'Kryst', 'E822002A', '4304B333', '4217999A', '41A0D70B', 'BF5DB3D0', '00000000', '00000000', 'BF000000'),
	(25, 'Teletown - Lin', 'Lin', 'DC3C0011', '426EE148', '412C624E', '419069D0', 'BEB77C03', '00000000', '00000000', 'BF6EFF19'),
	(26, 'Teletown - Linvak Tukal', 'Linvak Tukal', 'A21E001A', '42A60000', '42180000', '440C1733', '3F800000', '00000000', '00000000', '00000000'),
	(27, 'Teletown - Lytelthorpe', 'Lytelthorpe', 'C0800007', '413B9168', '431B8F5C', '42041CC2', 'BECE0286', '00000000', '00000000', 'BF6A5CE6'),
	(28, 'Teletown - MacNiall\'s Freehold', 'MacNiall\'s Freehold', 'F224001A', '42A3999A', '42040000', '3BA3D70A', '3E76DC5D', '00000000', '00000000', 'BF787326'),
	(29, 'Teletown - Mayoi', 'Mayoi', 'E6320021', '42D6D581', '412C353F', '41EF433E', 'BF248DC1', '00000000', '00000000', 'BF441B76'),
	(30, 'Teletown - Nanto', 'Nanto', 'E63E0022', '42C1EB85', '4216E354', '42952625', '00000000', '00000000', '00000000', 'BF800000'),
	(31, 'Teletown - Neydisa', 'Neydisa', '95D60033', '4312E666', '428E999A', '42C786D4', 'BF3B3A04', '00000000', '00000000', 'BF2E976C'),
	(32, 'Teletown - Oolutanga\'s Refuge', 'Oolutanga\'s Refuge', 'F6820033', '4311B333', '42476B85', '4268051F', 'BEEF61ED', '00000000', '00000000', 'BF624BDC'),
	(33, 'Teletown - Plateau Village', 'Plateau Village', '49B70021', '42C83333', '41A66666', '436E9D03', 'BF167914', '00000000', '00000000', 'BF4F1BBD'),
	(34, 'Teletown - Qalaba\'r', 'Qalaba\'r', '9722003A', '43285AA0', '41C4F1AA', '42CC0290', 'BF6C3BF7', '00000000', '00000000', 'BEC5464E'),
	(35, 'Teletown - Redspire', 'Redspire', '17B2002A', '43049F7D', '41CE78D5', '4230051F', '3F7F9C95', '00000000', '00000000', 'BD6189BE'),
	(36, 'Teletown - Rithwic', 'Rithwic', 'C98C0028', '42E354CA', '433E424E', '41B00A3D', 'BF3504F7', '00000000', '00000000', 'BF3504F7'),
	(37, 'Teletown - Samsur', 'Samsur', '977B000C', '41CE7CEE', '4293B4BC', '3BA3D70A', '3F6E1134', '00000000', '00000000', 'BEBC4157'),
	(38, 'Teletown - Sawato', 'Sawato', 'C95B0001', '416CCCCD', '3E99999A', '4140147A', '3F6E2FE0', '00000000', '00000000', 'BEBBA605'),
	(39, 'Teletown - Shoushi', 'Shoushi', 'DA55001D', '42A9999A', '42C60000', '41A00A3D', '3F800000', '00000000', '00000000', '00000000'),
	(40, 'Teletown - Singularity Caul', 'Singularity Caul Island', '09040008', '41366666', '433C999A', '42AF0B18', 'BF7F1077', '00000000', '00000000', 'BDAEEF1C'),
	(41, 'Teletown - Stonehold', 'Stonehold', '64D5000B', '41F00000', '42480000', '429C0290', '3F57E879', '00000000', '00000000', 'BF098C7E'),
	(42, 'Teletown - Timaru', 'Timaru', '1DB60025', '42C50000', '42C43333', '42F00290', '3F4F5EEA', '00000000', '00000000', 'BF161C7A'),
	(43, 'Teletown - Tou-Tou', 'Tou-Tou', 'F65C002B', '42FCC625', '42589687', '41A00A3D', '3F6DBBAE', '00000000', '00000000', 'BEBDEFE9'),
	(44, 'Teletown - Tufa', 'Tufa', '876C0008', '40000000', '433AE666', '41900A3D', 'BF3504F7', '00000000', '00000000', 'BF3504F7'),
	(45, 'Teletown - Underground City', 'Underground City', '01E901AD', '42F00000', 'C3020000', 'C13FEB86', 'BF36E47E', '00000000', '00000000', 'BF332064'),
	(46, 'Teletown - Uziz', 'Uziz', 'A260003C', '4336EB44', '42AFDE35', '41A00A3D', 'BEBA17D3', '00000000', '00000000', 'BF6E7DED'),
	(47, 'Teletown - Wai Jhou', 'Wai Jhou', '3F31001F', '42A6334D', '431C310A', '4031EC53', '3F0A3AD2', '00000000', '00000000', '3F577910'),
	(48, 'Teletown - Xarabydun', 'Xarabydun', '934B0021', '42D899D2', '40C32B22', '4191273D', 'BF76ED35', '00000000', '00000000', 'BE871A5D'),
	(49, 'Teletown - Yanshi', 'Yanshi', 'B46F001E', '42966666', '42F83333', '420AC0DB', '3F800000', '00000000', '00000000', '00000000'),
	(50, 'Teletown - Yaraq', 'Yaraq', '7D64000D', '41FF3333', '42D13333', '413F258C', '3F13BFC6', '00000000', '00000000', 'BF510F73'),
	(51, 'Teletown - Zaikhal', 'Zaikhal', '80900013', '4281B9DB', '425EBF7D', '42F80290', 'BF6E0CBF', '00000000', '00000000', 'BEBC57C1'),
	(52, 'Teletown - Eastwatch', 'Eastwatch', '49F00023', '42d80000', '42700000', '432a0000', '3f800000', '00000000', '00000000', '00000000'),
	(53, 'Teletown - Westwatch', 'Westwatch', '23DA002B', '430535c3', '428ce148', '4000a3d7', 'bf7d70a4', '00000000', '00000000', '00000000'),
	(54, 'Teletown - Silyun', 'Silyun', '27EC0015', '42700000', '42d80000', '42a0051f', '3f800000', '00000000', '00000000', '00000000'),
	(55, 'Teletown - Sanamar', 'Sanamar', '33D9000B', '423b51ec', '42687ae1', '42500a3d', '3f800000', '00000000', '00000000', '3f800000'),
	(56, 'Teletown - Merwart Village', 'Merwart Village', 'C9E3000B', '42100000', '2700000', '41600000', '3f800000', '00000000', '00000000', 'bf800000'),
	(57, 'Teletown - Fiun Outpost', 'Fiun Outpost', '38F7001A', '42a0e666', '41fce148', '3faf5c29', '00000000', '00000000', '00000000', '3f800000'),
	(58, 'Teletown - Kor-Gursha', 'Kor-Gursha', '009C0247', '43200000', 'c28c0000', 'c1900000', '3f800000', '00000000', '00000000', 'bf800000'),
	(59, 'Teletown - Mar\'uun', 'Mar\'uun', '880401C4', '42200000', 'c3020000', 'c3700000', '3f800000', '00000000', '00000000', '00000000');
/*!40000 ALTER TABLE `teletowns` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
