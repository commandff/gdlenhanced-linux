-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gdle
CREATE DATABASE IF NOT EXISTS `gdle` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gdle`;

-- Dumping structure for table gdle.accounts
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(129) NOT NULL,
  `password_salt` varchar(17) NOT NULL,
  `date_created` int(11) NOT NULL COMMENT 'unix timestamp',
  `access` int(11) NOT NULL COMMENT '0=basic 3=advocate 4=sentinel 6=admin',
  `created_ip_address` varchar(64) NOT NULL,
  `email` varchar(64) DEFAULT '',
  `emailsetused` bit(1) NOT NULL DEFAULT b'0',
  `banned` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=964 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for procedure gdle.blob_update_house
DROP PROCEDURE IF EXISTS `blob_update_house`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `blob_update_house`(
    IN houseId INT(11),
    IN houseData LONGBLOB
)
BEGIN

INSERT INTO houses (house_id, DATA)
    VALUES (houseId, houseData)
    ON DUPLICATE KEY UPDATE
        DATA=houseData;
END//
DELIMITER ;

-- Dumping structure for procedure gdle.blob_update_weenie
DROP PROCEDURE IF EXISTS `blob_update_weenie`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `blob_update_weenie`(
	IN `weenieId` INT(11) UNSIGNED,
	IN `topLevelId` INT(11) UNSIGNED,
	IN `blockId` INT(11) UNSIGNED,
	IN `weenieData` MEDIUMBLOB



,
	IN `weeniets` INT
)
BEGIN

DECLARE new_hash VARCHAR(32);
DECLARE old_hash VARCHAR(32);


IF NOT EXISTS (SELECT id FROM weenies WHERE id = weenieId) THEN
	INSERT INTO weenies (id, top_level_object_id, block_id, DATA, weenie_ts)
   	 VALUES (weenieId, topLevelId, blockId, weenieData, weeniets);
ELSE
	SET new_hash = MD5(MID(weenieData, 9, LENGTH(weenieData)-8));
	SELECT MD5(MID(`DATA`, 9, LENGTH(`DATA`)-8)) INTO old_hash FROM weenies WHERE id = weenieId;
	IF new_hash <> old_hash THEN
		UPDATE weenies
			SET top_level_object_id = topLevelId, block_id = blockId, `DATA` = weenieData, weenie_ts=weeniets
			WHERE id = weenieId;
   END IF;
END IF;

END//
DELIMITER ;

-- Dumping structure for table gdle.blocks
DROP TABLE IF EXISTS `blocks`;
CREATE TABLE IF NOT EXISTS `blocks` (
  `block_id` int(11) unsigned NOT NULL,
  `weenie_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`weenie_id`),
  KEY `block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.characters
DROP TABLE IF EXISTS `characters`;
CREATE TABLE IF NOT EXISTS `characters` (
  `account_id` int(11) NOT NULL,
  `weenie_id` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_created` int(11) NOT NULL,
  `instance_ts` int(11) NOT NULL,
  `ts_deleted` int(11) NOT NULL DEFAULT 0,
  `ts_login` int(11) NOT NULL DEFAULT 0,
  `gag_ts` double DEFAULT 0,
  `gag_timer` double DEFAULT 0,
  `gag_count` int(11) DEFAULT 0,
  UNIQUE KEY `weenie_id` (`weenie_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.character_corpses
DROP TABLE IF EXISTS `character_corpses`;
CREATE TABLE IF NOT EXISTS `character_corpses` (
  `character_id` int(10) unsigned NOT NULL,
  `corpses` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`character_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.character_friends
DROP TABLE IF EXISTS `character_friends`;
CREATE TABLE IF NOT EXISTS `character_friends` (
  `character_id` int(11) unsigned NOT NULL,
  `friend_type` int(11) unsigned NOT NULL,
  `friend_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`character_id`,`friend_type`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.character_squelch
DROP TABLE IF EXISTS `character_squelch`;
CREATE TABLE IF NOT EXISTS `character_squelch` (
  `character_id` int(11) unsigned NOT NULL DEFAULT 0,
  `squelched_id` int(11) unsigned NOT NULL DEFAULT 0,
  `account_id` int(11) unsigned NOT NULL DEFAULT 0,
  `isip` bit(1) DEFAULT b'0',
  `isspeech` bit(1) DEFAULT b'0',
  `istell` bit(1) DEFAULT b'0',
  `iscombat` bit(1) DEFAULT b'0',
  `ismagic` bit(1) DEFAULT b'0',
  `isemote` bit(1) DEFAULT b'0',
  `isadvancement` bit(1) DEFAULT b'0',
  `isappraisal` bit(1) DEFAULT b'0',
  `isspellcasting` bit(1) DEFAULT b'0',
  `isallegiance` bit(1) DEFAULT b'0',
  `isfellowhip` bit(1) DEFAULT b'0',
  `iscombatenemy` bit(1) DEFAULT b'0',
  `isrecall` bit(1) DEFAULT b'0',
  `iscrafting` bit(1) DEFAULT b'0',
  PRIMARY KEY (`character_id`,`squelched_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table gdle.character_titles
DROP TABLE IF EXISTS `character_titles`;
CREATE TABLE IF NOT EXISTS `character_titles` (
  `character_id` int(10) unsigned NOT NULL,
  `titles` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`character_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.character_windowdata
DROP TABLE IF EXISTS `character_windowdata`;
CREATE TABLE IF NOT EXISTS `character_windowdata` (
  `character_id` int(11) NOT NULL DEFAULT 0,
  `bloblength` int(11) DEFAULT NULL,
  `windowblob` blob DEFAULT NULL,
  PRIMARY KEY (`character_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `setting` varchar(50) NOT NULL,
  `runtime` varchar(50) DEFAULT NULL,
  `base` varchar(50) NOT NULL,
  `gameuse` varchar(500) NOT NULL,
  KEY `setting` (`setting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.globals
DROP TABLE IF EXISTS `globals`;
CREATE TABLE IF NOT EXISTS `globals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13658 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.houses
DROP TABLE IF EXISTS `houses`;
CREATE TABLE IF NOT EXISTS `houses` (
  `house_id` int(11) NOT NULL AUTO_INCREMENT,
  `data` longblob NOT NULL,
  PRIMARY KEY (`house_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6251 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table gdle.teletowns
DROP TABLE IF EXISTS `teletowns`;
CREATE TABLE IF NOT EXISTS `teletowns` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  `Command` varchar(50) NOT NULL,
  `Landblock` varchar(50) NOT NULL,
  `Position_X` varchar(50) NOT NULL,
  `Position_Y` varchar(50) NOT NULL,
  `Position_Z` varchar(50) NOT NULL,
  `Orientation_W` varchar(50) NOT NULL,
  `Orientation_X` varchar(50) NOT NULL,
  `Orientation_Y` varchar(50) NOT NULL,
  `Orientation_Z` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for view gdle.vwgetconfig
DROP VIEW IF EXISTS `vwgetconfig`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vwgetconfig` (
	`lcase(setting)` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`value` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view gdle.vwweenieorphants
DROP VIEW IF EXISTS `vwweenieorphants`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vwweenieorphants` (
	`CharacterId` INT(11) UNSIGNED NOT NULL,
	`MaxTimestamp` INT(22) NULL,
	`MinTimestamp` INT(22) NULL
) ENGINE=MyISAM;

-- Dumping structure for table gdle.weenies
DROP TABLE IF EXISTS `weenies`;
CREATE TABLE IF NOT EXISTS `weenies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `top_level_object_id` int(11) unsigned NOT NULL,
  `block_id` int(11) unsigned NOT NULL,
  `data` mediumblob NOT NULL,
  `weenie_ts` int(22) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `top_level_object_id` (`top_level_object_id`),
  KEY `block_id` (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2416331541 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for view gdle.vwgetconfig
DROP VIEW IF EXISTS `vwgetconfig`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vwgetconfig`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `vwgetconfig` AS SELECT lcase(setting), COALESCE(runtime, base, 0) as value from config ;

-- Dumping structure for view gdle.vwweenieorphants
DROP VIEW IF EXISTS `vwweenieorphants`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vwweenieorphants`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `vwweenieorphants` AS Select id as CharacterId,
		 TimeStamps.MaxTS as MaxTimestamp,
		 TimeStamps.MinTS as MinTimestamp
from weenies,
(select top_level_object_id, max(weenie_ts) as MaxTS, min(weenie_ts) as MinTS from weenies where top_level_object_id < 1610612736 group by top_level_object_id)
as TimeStamps
where weenies.id = TimeStamps.top_level_object_id
and id < 1610612736 and TimeStamps.MaxTS != TimeStamps.MinTS and TimeStamps.MaxTS > 0
group by id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
