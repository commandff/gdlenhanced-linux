

-- grants access to gdle, using a .sock ONLY
-- This is the password used from the gdle game server
GRANT ALL PRIVILEGES ON `gdle`.* TO 'gdle'@'localhost' IDENTIFIED BY 'ChangeThisTooBecauseISaidSoAlsoMyFavoriteColorIsGrey.';
-- grants access to gdle, using a TCP:3306 or .sock
-- This is the password used from phpmyadmin (outward facing)
GRANT ALL PRIVILEGES ON `gdle`.* TO 'gdle'@'%' IDENTIFIED BY 'ChangeThisTooBecauseISaidSoAlsoMyFavoriteColorIsGrey.';


-- Leave these commented, unless you specifically need root access
-- -- grants access to root, using a .sock ONLY
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'ChangeThisOrIWillDyeYourCatPink' WITH GRANT OPTION;
-- -- grants access to root, using a TCP:3306 or .sock
-- -- This is the password used from phpmyadmin (outward facing)
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'ChangeThisOrIWillDyeYourCatPink' WITH GRANT OPTION;


FLUSH PRIVILEGES;
