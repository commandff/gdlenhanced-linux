Database is generated from the .sql files in this folder, in alphabetical order.

- `50_gdledb_newsetup.sql` exact copy from GDLE137-1.zip:sqldumps/gdledb_newsetup.sql
- `51_gdledb_newsetup_data.sql` copy from GDLE137-1.zip:sqldumps/gdledb_newsetup_data.sql, with a bug fix at line 15 (use db missing)
- `99_grants.sql` All SQL users and passwords are configured here.
- `README.md` Since this file does not end in .sql, it probably won't be imported.
