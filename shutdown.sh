#!/bin/sh
echo "*** Shutting down GDLE..."
docker stop gdle
echo "*** Server down. Waiting 5 seconds for DB to flush"
sleep 5
echo "*** Shutting down phpmyadmin and mariadb"
docker stop pma mariadb
sleep 1
echo "*** Server shutdown complete."
