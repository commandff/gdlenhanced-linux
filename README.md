# Shopping List
1. A server or vm capable of running docker containers. If the environment does not support docker-compose, you will need to configure the containers manually, using the docker-compose.yml as a guide.
2. ac_data.zip - pulled from your own licensed copy of the game.
3. GDLE137-1.zip - https://www.gdleac.com/releases/1-37-1-a-few-crashes-later/
4. GDLE1_38.zip - https://www.gdleac.com/releases/1-38-movement-happening/
5. GDLEnhanced.tar.bz2 - TBD (will be published soon:tm:)
6. LSD-Partial-2022-04-06_12-00.zip - https://lifestoned.net/WorldRelease


# Windows 10 Docker Desktop/WSL2 system setup
- Install Git Bash https://gitforwindows.org/
- Install Docker https://docs.docker.com/desktop/windows/install/
  - Ensure `Use WSL2 instead of Hyper-V (recommended)` is selected.
- In git bash, Clone this repo
  - `GDLE@FUNKY MINGW64 /c/Users/GDLE (main)$ git clone https://gitlab.com/commandff/gdlenhanced-linux.git server`
- Place Shopping List items #2-6 in home folder (/c/Users/GDLE)
- Follow user setup steps, starting at #5.

# Ubuntu Server 22.04 system setup (as root)
Initial development and testing has been performed on Ubuntu Server 22.04. Other operating systems will very likely work, but have not been tested yet.
```sh
# install basic pre-reqs
apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release libmariadb3 bzip2 unzip jq

# setup ubuntu docker repo
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update

# install docker
apt install docker-ce docker-compose
# create `gdle` user
adduser gdle
# add `gdle` user to the `docker` group
usermod -aG docker gdle
```

# Ubuntu Server 22.04 user setup (as gdle)
#### 1. Clone this repo
```sh
git clone https://gitlab.com/commandff/gdlenhanced-linux.git server

#alternatively, auth this clone, for container development
ssh-keygen -b 4096
#upload `~/.ssh/id_rsa.pub` to your gitlab account under SSH Keys, for authentication
git clone git@gitlab.com:<yourusername>/gdlenhanced-linux.git server
```

#### 2. Generate ssh key on windows, and copy the contents of the public key
```sh
ssh-keygen -b 4096
cat ~/.ssh/id_rsa.pub
```
#### 3. Paste public key into `~/.ssh/authorized_keys` on server
```sh
gdle@funkydb:~$ echo 'ssh-rsa AAAAB...pw== Yonneh@AMDR9' >>~/.ssh/authorized_keys
```

#### 4. Transfer files to the server. Example is from Git Bash on windows
I would recommend setting up WIN-SSHFS (instructions towards the bottom of this page), and copy the files over via windows file explorer. This is just the "easy copy-pasta method"
```sh
export GDLE=gdle@192.168.50.175
scp GDLEnhanced.tar.bz2 $GDLE:/home/gdle/
scp GDLE137-1.zip $GDLE:/home/gdle/
scp GDLE1_38.zip $GDLE:/home/gdle/
scp LSD-Partial-2022-04-06_12-00.zip $GDLE:/home/gdle/
scp ac_data.zip $GDLE:/home/gdle/ # this one takes a minute.
```

#### 5. Extract user-supplied files on server

```sh
mkdir -p ~/server/gdle/Data/json/
unzip GDLE137-1.zip -d ~/server/gdle/
unzip -o GDLE1_38.zip -d ~/server/gdle/
unzip -o LSD-Partial-2022-04-06_12-00.zip -d ~/server/gdle/Data/json/
tar xvfC GDLEnhanced.tar.bz2 ~/server/gdle/
unzip -o ac_data.zip -d ~/server/gdle/Data/ client_cell_1.dat client_portal.dat
# cleanup archives
rm *.zip
```
- Directory structure should resemble this.
```
./server/
./server/docker-compose.yml
./server/gdle/Data/cache.bin
./server/gdle/Data/client_cell_1.dat
./server/gdle/Data/client_portal.dat
./server/gdle/Data/json/...
./server/gdle/Data/worlddesc
./server/gdle/GDLEnhanced
./server/gdle/server.cfg
./server/README.md (this file)
./server/restart.sh
./server/shutdown.sh
./server/sqldumps
./server/sqldumps/50_gdledb_newsetup.sql
./server/sqldumps/51_gdledb_newsetup_data.sql
./server/sqldumps/99_grants.sql
./server/startup.sh
./server/webhooks
./server/webhooks/echo.sh
./server/webhooks/gitlab.sh
```

#### 6. Verify checksums
```sh
cd ~/server/gdle/Data/
sha256sum -c - <<EOF
6db0abf00fbceed62c3f1ee842ee7c1f423d732bed77a5b7c102ee89a52ab99e  client_cell_1.dat
dc6e500ba22e6b186db7171e3f3345238b6444c85d798adc85e550973b8d12e4  client_portal.dat
EOF
```
```
client_cell_1.dat: OK
client_portal.dat: OK
```

#### 7. update server.cfg
```sh
pico ~/server/gdle/server.cfg
```
database_ip needs to be the exact string `localhost`, as DB communications will be handled by a unix socket. `database_password` needs to match what is set in `sqldumps/99_grants.sql`
- `database_ip=localhost`
- `database_username=gdle`
- `database_port=0`
- `database_password=ChangeThisTooBecauseISaidSoAlsoMyFavoriteColorIsGrey.`
- `database_name=gdle`
- `world_name=GDLEnhanced Linux`
- ...
- `auto_create_accounts=1`

*(ctrl+o, enter, ctrl+x to save and exit)*

#### 8. build and start containers.
- phpmyadmin does not need to run 24/7. It can be started and stopped as needed.
- This will create a lot of spam. especially on the first run while the db is generated. Reading is optional.
> ```sh
> cd ~/server
> docker-compose up mariadb pma gdle
> ```
> - `pma        | [Wed Jun 15 01:12:49.285553 2022] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'`
> - `mariadb    | 2022-06-15  0:55:05 0 [Note] mysqld: ready for connections.`
> - `gdle       | 2022-06-15 01:15:30,605 | INFO | Server | The server is now online.`
> - *(ctrl+\ to break docker-compose script and keep container running. or ctrl+c to kill container if something went wrong)*

# phpmyadmin
Only really needed to setup admin accounts.
- `http://[server-ip]:8900`
- Username: `gdle`
- Password: `ChangeThisTooBecauseISaidSoAlsoMyFavoriteColorIsGrey.`

# usefull commands
- `docker ps -a`: Show running containers (should be pma, mariadb, and gdle)
- `docker stop gdle`: Shut down GDLE Server.
  - If GDLE does not close down properly, or is otherwise not responding, kill it with `docker kill gdle`. careful, this has a moderate chance of corrupting weenies.
- `docker stop pma`: Shut down phpmyadmin
- `docker stop mariadb`: Shut down mariadb
- `docker start mariadb`: Start mariadb
- `docker start pma`: Start phpmyadmin
- `docker start gdle`: Start GDLE Server
- `docker logs -n 100 -f gdle`: follow server STDOUT *(ctrl+c to stop- does not kill server)*
- `docker stats`: Monitor memory/network/disk I/O *(ctrl+c to stop- does not kill server)*
- `~/server/restart.sh`: Restarts just the game server (for content updates, memory issues, etc)
- `~/server/shutdown.sh`: Properly shuts down the game server, phpmyadmin, and mariadb, to prepare for system shutdown/restart
- `~/server/startup.sh`: Properly starts mariadb and the game server - does nothing if they are already marked as running.

# Database Snapshot
This snapshot can be used to rapidly restore a server to a known state. It takes considerably more space than a .sql export, but can be thousands of times faster. This is intended for batch testing, but can still be used to backup a production database.

These commands should be run while mariadb is not running. The aria log and ib buffer pool are also backed up with this method.

### Backup
```sh
cd ~/server/db
tar cjf ~/latest_db_backup.tar.bz2 .
```

### Wipe database
```sh
rm -r ~/server/db
```

### Restore
```sh
cd ~/server/db
tar xjf ~/latest_db_backup.tar.bz2
```

# Reset Button
The following series of commands will stop and remove the containers, and reset the database.
```sh
~/server/shutdown.sh
docker rm gdle mariadb pma webhookd
rm -r ~/server/db
```

# File Access Sanity (SSHFS-WIN)
For a bit of sanity, and not having to jump through flaming hoops to transfer files, SSHFS is an option.
1. See above, to generate an ssh key on windows, and add it to your `~/.ssh/authorized_keys` file.
2. Install WINFSP https://github.com/winfsp/winfsp/releases/download/v1.10/winfsp-1.10.22006.msi
3. Install SSHFS-WIN https://github.com/winfsp/sshfs-win/releases/download/v3.7.21011/sshfs-win-3.7.21011-x86.msi
4. In Git Bash, enter the command `net use X: \\\\sshfs.k\\gdle@192.168.50.175` to mount gdle's home directory on `192.168.50.175`, as `/x/` in windows. You can then open the folders directly in your preferred editing platform.

# webhookd
- WIP
- intended to capture gitlab/github events, and react appropriately.
  - content repo merge - perform a git pull in the content folder
  - content repo tag - perform a git pull in the content folder, and restart the server.
  - binary server repo update - perform a git pull in the server folder, and restart the server.
