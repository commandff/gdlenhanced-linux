#!/bin/sh
echo "*** Restarting GDLE..."
docker stop gdle
echo "*** Server down. Waiting 5 seconds for DB to flush"
sleep 5
docker start gdle
echo "*** GDLE Started. use \"docker logs -n 100 -f gdle\" to monitor it's progress."
